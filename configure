#!/usr/bin/env bash

# Default configuration
version="unknown"
prefix="/usr/local"
winepath="/opt/wine-compholio/bin/wine"
mozpluginpath="/usr/lib/mozilla/plugins"
gccruntimedlls=""
cxx=""
win32cxx=""
win32flags="-m32"
win64="false"
win64cxx=""
win64flags="-m64"
quietinstallation="true"
nogpuaccel="false"
debug="false"

usage ()
{
	echo ""
	echo "Usage: ./configure [--prefix=PREFIX] [--wine-path=PATH] [--debug]"
	echo "                   [--moz-plugin-path=PATH] [--gcc-runtime-dlls=PATH]"
	echo "                   [--show-installation-dialogs] [--no-gpu-accel]"
	echo "                   [--cxx]"
	echo " 32-bit options:"
	echo "                   [--win32-prebuilt] [--win32-cxx=COMPILER]"
	echo "                   [--win32-static] [--win32-flags=FLAGS]"
	echo " 64 bit options:"
	echo "                   [--with-win64]"
	echo "                   [--win64-prebuilt] [--win64-cxx=COMPILER]"
	echo "                   [--win64-static] [--win64-flags=FLAGS]"
	echo ""
}

while [[ $# > 0 ]] ; do
	CMD="$1"; shift
	case "$CMD" in
		--prefix=*)
			prefix="${CMD#*=}"
			;;
		--prefix)
			prefix="$1"; shift
			;;

		--wine-path=*)
			winepath="${CMD#*=}"
			;;
		--wine-path)
			winepath="$1"; shift
			;;

		--moz-plugin-path=*)
			mozpluginpath="${CMD#*=}"
			;;
		--moz-plugin-path)
			mozpluginpath="$1"; shift
			;;

		--gcc-runtime-dlls=*)
			gccruntimedlls="${CMD#*=}"
			;;
		--gcc-runtime-dlls)
			gccruntimedlls="$1"; shift
			;;

		--show-installation-dialogs)
			quietinstallation="false"
			;;

                --cxx=*)
                        cxx="${CMD#*=}"
                        ;;
                --cxx)
                        cxx="$1"; shift
                        ;;

		--debug)
			debug="true"
			;;

		--win32-static)
			win32flags="$win32flags -static-libgcc -static-libstdc++ -static"
			;;
		--win32-prebuilt)
			win32cxx="prebuilt"
			;;

		--win32-cxx=*)
			win32cxx="${CMD#*=}"
			;;
		--win32-cxx)
			win32cxx="$1"; shift
			;;

		--win32-flags=*)
			win32flags="$win32flags ${CMD#*=}"
			;;
		--win32-flags)
			win32flags="$win32flags $1"; shift
			;;

		--with-win64)
			win64="true"
			;;

		--win64-static)
			win64flags="$win64flags -static-libgcc -static-libstdc++ -static"
			;;
		--win64-prebuilt)
			win64cxx="prebuilt"
			;;

		--win64-cxx=*)
			win64cxx="${CMD#*=}"
			;;
		--win64-cxx)
			win64cxx="$1"; shift
			;;

		--win64-flags=*)
			win64flags="$win64flags ${CMD#*=}"
			;;
		--win64-flags)
			win64flags="$win64flags $1"; shift
			;;

		--no-gpu-accel)
			nogpuaccel="true"
			;;

		--help)
			usage
			exit
			;;
		*)
			echo "WARNING: Unknown argument $CMD." >&2
			;;
	esac
done

# Try to autodetect compiler
if [ -z "$cxx" ]; then
        if command -v g++ >/dev/null 2>&1; then
                cxx="g++"
        elif command -v clang++ > /dev/null 2>&1; then
                cxx="clang++"
        else
                echo "ERROR: No cxx compiler found. Please use --cxx to specify one."
                exit 1
        fi
fi

if [ -z "$win32cxx" ]; then
	if command -v i686-w64-mingw32-g++ >/dev/null 2>&1; then
		win32cxx="i686-w64-mingw32-g++"
		win32flags="$win32flags -static-libgcc -static-libstdc++ -static"

        elif command -v mingw32-g++ > /dev/null 2>&1; then
                win32cxx="mingw32-g++"
                win32flags="$win32flags -DMINGW32_FALLBACK -static-libgcc -static-libstdc++ -static"

	elif command -v i686-pc-mingw32-g++ > /dev/null 2>&1; then
		win32cxx="i686-pc-mingw32-g++"
		win32flags="$win32flags -DMINGW32_FALLBACK -static-libgcc -static-libstdc++ -static"

	elif command -v i586-mingw32msvc-c++ > /dev/null 2>&1; then
		win32cxx="i586-mingw32msvc-c++"
		win32flags="$win32flags -DMINGW32_FALLBACK -static-libgcc -static-libstdc++ -static"

	elif command -v wineg++ > /dev/null 2>&1; then
		win32cxx="wineg++"

	else
		echo "ERROR: No mingw32-g++ compiler found. Please use --win32-cxx to specify one."
		exit 1
	fi
fi

if [ "$win64" == "true" ] && [ -z "$win64cxx" ]; then
	if command -v x86_64-w64-mingw32-g++ >/dev/null 2>&1; then
		win64cxx="x86_64-w64-mingw32-g++"
		win64flags="$win64flags -static-libgcc -static-libstdc++ -static"

	else
		echo "ERROR: No mingw64-g++ compiler found. Please use --win64-cxx to specify one."
		exit 1
	fi
fi

# Get the version number
changelog=$(head -n1 debian/changelog)
if [[ "$changelog" =~ \((.*)\)\ (UNRELEASED)? ]]; then
	version="${BASH_REMATCH[1]}"
	if [ "${BASH_REMATCH[2]}" == "UNRELEASED" ]; then
		version="$version-daily"
	fi
fi

# Use realpath instead of readlink if possible (*BSD and recent linux versions)
if ! command -v realpath >/dev/null 2>&1; then
	resolvepath()
	{
		readlink -m "$1"
	}
else
	resolvepath()
	{
		if [ ! -e "$1" ]; then
			echo "$1"
		else
			realpath "$1"
		fi
	}
fi

# Normalize the paths
prefix=$(resolvepath "$prefix")
winepath=$(resolvepath "$winepath")
mozpluginpath=$(resolvepath "$mozpluginpath")
gccruntimedlls=$(resolvepath "$gccruntimedlls")

echo "Configuration Summary"
echo "---------------------"
echo ""
echo "Pipelight has been configured with"
echo " version           = '$version'"
echo " prefix            = '$prefix'"
echo " winepath          = '$winepath'"
echo " mozpluginpath     = '$mozpluginpath'"
echo " gccruntimedlls    = '$gccruntimedlls'"
echo " quietinstallation = '$quietinstallation'"
echo " cxx               = '$cxx'"
echo " win32cxx          = '$win32cxx'"
echo " win32flags        = '$win32flags'"
echo " win64             = '$win64'"
echo " win64cxx          = '$win64cxx'"
echo " win64flags        = '$win64flags'"
echo " nogpuaccel        = '$nogpuaccel'"
echo " debug             = '$debug'"
echo ""
echo "IMPORTANT: Please ensure you have XATTR support enabled for both wine and"
echo "           your file system (required to watch DRM protected content)!"
echo ""

echo "version=$version"						>  config.make
echo "prefix=$prefix"						>> config.make
echo "winepath=$winepath"					>> config.make
echo "mozpluginpath=$mozpluginpath"			>> config.make
echo "gccruntimedlls=$gccruntimedlls"		>> config.make
echo "quietinstallation=$quietinstallation" >> config.make
echo "cxx=$cxx"					>> config.make
echo "win64=$win64"							>> config.make
echo "win64cxx=$win64cxx"					>> config.make
echo "win64flags=$win64flags"				>> config.make
echo "win32cxx=$win32cxx"					>> config.make
echo "win32flags=$win32flags"				>> config.make
echo "nogpuaccel=$nogpuaccel"				>> config.make
echo "debug=$debug"							>> config.make

exit 0
